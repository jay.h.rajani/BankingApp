# Banking App

### Description

This project provides APIs for a bank to open a secondary account for existing customers.

There are three customers preloaded using db initialization script.

The user of the API can also list accounts owned by a customer and can read details of a specific account.

I also provided very limited money transfer functionality in order to test account-transaction relationship.

The APIs contains three main entities
* Customer
* Account
* Transaction

### Tools used

1. Java 8
2. Spring Boot 2
3. H2 - In memory embedded database
4. Swagger - Documentation. Easy way to call APIs instead of writing frontend
5. JPA
6. Tomcat - Embedded Web Server
7. Cucumber - Integration Test
8. Jackson - Json (De)serialization

### Assumptions

1. Security is considered out of scope for this application
2. All the amounts are in Integer to keep API simple
3. Customer can create as many accounts as he wants

### How to run the app

Once you run the application, you can use Swagger console to invoke different APIs

> Maven
```bash
mvn spring-boot:run
```

> Eclipse/IntelliJ
Run com.rajani.exercise.BankingApplication as standalone Java application

### Links

* [H2 Database Console](http://localhost:8080/console/)
* [Swagger](http://localhost:8080/swagger-ui.html)

### H2 Database

Please following values to connect to H2 database

![H2 Console](doc/images/H2_database_connection.png)

### Time Spent

Roughly 7 hours spread across 3 days

### Sonar

> Maven
```bash
mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dmaven.test.failure.ignore=false sonar:sonar
```