package com.rajani.exercise.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jayrajani
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Customer not found.")
public class CustomerNotFoundException extends RuntimeException {
}
