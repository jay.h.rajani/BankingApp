package com.rajani.exercise.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jayrajani
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Account not found.")
public class AccountNotFoundException extends RuntimeException {
}
