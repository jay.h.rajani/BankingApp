package com.rajani.exercise.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jayrajani
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Amount is not valid.")
public class InvalidAmountException extends RuntimeException {
}
