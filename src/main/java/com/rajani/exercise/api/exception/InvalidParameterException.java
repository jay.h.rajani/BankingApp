package com.rajani.exercise.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jayrajani
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Account not found.")
public class InvalidParameterException extends RuntimeException {

    public InvalidParameterException() {

    }

    public InvalidParameterException(String message) {
        super(message);
    }
}
