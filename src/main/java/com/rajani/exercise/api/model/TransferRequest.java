package com.rajani.exercise.api.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

/**
 * Created by jayrajani
 */
public class TransferRequest implements Serializable {

    @PositiveOrZero
    int amount;

    String description;

    @Positive
    long sender;

    @Positive
    long receiver;

    public TransferRequest() {
        // Default Constructor
    }

    public TransferRequest(int amount, long sender, long receiver) {
        this.amount = amount;
        this.sender = sender;
        this.receiver = receiver;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getSender() {
        return sender;
    }

    public void setSender(long sender) {
        this.sender = sender;
    }

    public long getReceiver() {
        return receiver;
    }

    public void setReceiver(long receiver) {
        this.receiver = receiver;
    }

    @Override
    public String toString() {
        return "TransferRequest{" +
                "amount=" + amount +
                ", description='" + description + '\'' +
                ", sender=" + sender +
                ", receiver=" + receiver +
                '}';
    }
}
