package com.rajani.exercise.api.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

/**
 * Created by jayrajani
 */
public class CreateAccountRequest implements Serializable {

    @Positive
    long customerId;

    @PositiveOrZero
    Integer initialBalance;

    public CreateAccountRequest() {
        // Default Constructor
    }

    public CreateAccountRequest(long customerId, int initialBalance) {
        this.customerId = customerId;
        this.initialBalance = initialBalance;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public Integer getInitialBalance() {
        return initialBalance;
    }

    public void setInitialBalance(Integer initialBalance) {
        this.initialBalance = initialBalance;
    }


    @Override
    public String toString() {
        return "CreateAccountRequest{" +
                "customerId=" + customerId +
                ", initialBalance=" + initialBalance +
                '}';
    }
}
