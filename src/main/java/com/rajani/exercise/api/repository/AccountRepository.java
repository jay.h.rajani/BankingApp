package com.rajani.exercise.api.repository;

import com.rajani.exercise.api.dao.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by jayrajani
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long>{

    @Query("SELECT account FROM Account account where account.customer.id = :customerId")
    List<Account> findAccountsByCustomerId(@Param("customerId") Long customerId);
}
