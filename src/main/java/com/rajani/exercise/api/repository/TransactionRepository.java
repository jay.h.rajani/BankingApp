package com.rajani.exercise.api.repository;

import com.rajani.exercise.api.dao.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jayrajani
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
}
