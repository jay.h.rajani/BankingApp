package com.rajani.exercise.api.repository;

import com.rajani.exercise.api.dao.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jayrajani
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
