package com.rajani.exercise.api.controller;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.exception.InvalidParameterException;
import com.rajani.exercise.api.model.CreateAccountRequest;
import com.rajani.exercise.api.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * Created by jayrajani
 */
@RestController
@RequestMapping("/accounts")
public class AccountController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);

    @Resource(name = "accountService")
    private AccountService accountService;

    @GetMapping(path = "/{accountId}")
    public Account getAccountById(@PathVariable("accountId") final long accountId) {
        LOG.info("getAccountById: [{}]", accountId);
        if(accountId < 0) {
            throw new InvalidParameterException("AccountId can not be negative.");
        }
        return accountService.getAccountsById(accountId);

    }

    @PostMapping
    public Account createAccount(@Valid @RequestBody final CreateAccountRequest request) {
        LOG.info("createAccount: [{}]", request);
        return accountService.createAccount(request);
    }
}
