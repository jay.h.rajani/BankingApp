package com.rajani.exercise.api.controller;

import com.rajani.exercise.api.model.TransferRequest;
import com.rajani.exercise.api.service.AccountService;
import com.rajani.exercise.api.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by jayrajani
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {
    private static final Logger LOG = LoggerFactory.getLogger(TransactionController.class);

    @Resource(name = "transactionService")
    public TransactionService transactionService;

    @Resource(name = "accountService")
    public AccountService accountService;

    @PostMapping
    public void transfer(@RequestBody final TransferRequest request) {
        LOG.info("transfer: [{}]", request);

        transactionService.transfer(request,
                accountService.getAccountsById(request.getSender()),
                accountService.getAccountsById(request.getReceiver()));

    }
}
