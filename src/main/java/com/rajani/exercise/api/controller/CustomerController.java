package com.rajani.exercise.api.controller;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.exception.InvalidParameterException;
import com.rajani.exercise.api.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by jayrajani
 */
@RestController
@RequestMapping("/customers")
public class CustomerController {
    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    @Resource(name = "accountService")
    private AccountService accountService;

    @GetMapping(path = "/{customerId}/accounts")
    public List<Account> getAccountsByCustomerId(@PathVariable("customerId") final long customerId) {
        LOG.info("getAccountByCustomerId: [{}]", customerId);
        if(customerId < 0) {
            throw new InvalidParameterException("CustomerId can not be negative.");
        }
        return accountService.getAccountByCustomerId(customerId);
    }

}
