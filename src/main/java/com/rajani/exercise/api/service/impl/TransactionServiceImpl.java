package com.rajani.exercise.api.service.impl;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.dao.model.Transaction;
import com.rajani.exercise.api.exception.InvalidAmountException;
import com.rajani.exercise.api.model.TransferRequest;
import com.rajani.exercise.api.repository.AccountRepository;
import com.rajani.exercise.api.repository.TransactionRepository;
import com.rajani.exercise.api.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by jayrajani
 */
@Service("transactionService")
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private AccountRepository accountRepository;

    public TransactionServiceImpl() {
        // Default Constructor
    }

    public TransactionServiceImpl(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public Transaction initialCredit(Account account, int amount) {
        Transaction transaction = new Transaction();
        transaction.setType(Transaction.Type.CREDIT);
        transaction.setDescription("Initial Credit");
        transaction.setAmount(amount);
        transaction.setAccount(account);
        return transaction;
    }

    @Override
    public void transfer(TransferRequest transferRequest, Account sender, Account receiver) {

        if(sender.getBalance() < transferRequest.getAmount()) {
            throw new InvalidAmountException();
        }

        Transaction creditTransaction = new Transaction(transferRequest.getAmount(), transferRequest.getDescription(), Transaction.Type.DEBIT, sender);
        sender.getTransactions().add(creditTransaction);

        Transaction debitTransaction = new Transaction(transferRequest.getAmount(), transferRequest.getDescription(), Transaction.Type.CREDIT, receiver);
        receiver.getTransactions().add(debitTransaction);

        sender.setBalance(sender.getBalance() - transferRequest.getAmount());
        accountRepository.save(sender);

        receiver.setBalance(receiver.getBalance() + transferRequest.getAmount());
        accountRepository.save(receiver);
    }




}
