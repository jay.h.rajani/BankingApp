package com.rajani.exercise.api.service;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.dao.model.Transaction;
import com.rajani.exercise.api.model.TransferRequest;

/**
 * Created by jayrajani
 */
public interface TransactionService {
    Transaction initialCredit(Account account, int amount);

    void transfer(TransferRequest transferRequest, Account sender, Account receiver);
}
