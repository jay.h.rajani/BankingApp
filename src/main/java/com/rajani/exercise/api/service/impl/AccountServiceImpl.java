package com.rajani.exercise.api.service.impl;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.dao.model.Customer;
import com.rajani.exercise.api.dao.model.Transaction;
import com.rajani.exercise.api.exception.AccountNotFoundException;
import com.rajani.exercise.api.exception.CustomerNotFoundException;
import com.rajani.exercise.api.model.CreateAccountRequest;
import com.rajani.exercise.api.repository.AccountRepository;
import com.rajani.exercise.api.repository.CustomerRepository;
import com.rajani.exercise.api.service.AccountService;
import com.rajani.exercise.api.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

/**
 * Created by jayrajani
 */
@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Resource(name = "transactionService")
    private TransactionService transactionService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public AccountServiceImpl() {
        // Default Constructor
    }

    public AccountServiceImpl(TransactionService transactionService, AccountRepository accountRepository, CustomerRepository customerRepository) {
        this.transactionService = transactionService;
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    public Account createAccount(CreateAccountRequest request) {
        Account account = new Account();

        account.setCustomer(findCustomerById(request.getCustomerId()));

        if(request.getInitialBalance() != null && request.getInitialBalance() > 0) {
            account.setBalance(request.getInitialBalance());

            final Transaction transaction = transactionService.initialCredit(account, request.getInitialBalance());
            account.getTransactions().add(transaction);
        }

        account = accountRepository.save(account);
        return account;
    }

    @Override
    public Account getAccountsById(long accountId) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if(accountOptional.isPresent()) {
            return accountOptional.get();
        } else {
            throw new AccountNotFoundException();
        }
    }

    @Override
    public List<Account> getAccountByCustomerId(long customerId) {
        findCustomerById(customerId);
        return accountRepository.findAccountsByCustomerId(customerId);
    }

    protected Customer findCustomerById(long customerId) {
        final Optional<Customer> customerOptional = customerRepository.findById(customerId);
        if(!customerOptional.isPresent())
            throw new CustomerNotFoundException();
        return customerOptional.get();
    }
}
