package com.rajani.exercise.api.service;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.model.CreateAccountRequest;

import java.util.List;

/**
 * Created by jayrajani
 */
public interface AccountService {
    Account createAccount(CreateAccountRequest request);
    Account getAccountsById(long accountId);
    List<Account> getAccountByCustomerId(long customerId);
}
