package com.rajani.exercise.api.service;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.dao.model.Transaction;
import com.rajani.exercise.api.exception.InvalidAmountException;
import com.rajani.exercise.api.model.TransferRequest;
import com.rajani.exercise.api.repository.AccountRepository;
import com.rajani.exercise.api.repository.TransactionRepository;
import com.rajani.exercise.api.service.impl.TransactionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static org.mockito.Matchers.any;

/**
 * Created by jayrajani
 */
@SpringBootTest
public class TransactionServiceTest {

    private TransactionService target;

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private AccountRepository accountRepository;

    @Before
    public void init() {
        accountRepository = Mockito.mock(AccountRepository.class);
        transactionRepository = Mockito.mock(TransactionRepository.class);
        target = new TransactionServiceImpl(transactionRepository, accountRepository);
    }

    @Test
    public void initialDepositTest() {

        Mockito.when(transactionRepository.save(any(Transaction.class))).then(AdditionalAnswers.returnsFirstArg());

        final Transaction transaction = target.initialCredit(getRandomAccount(), 10);
        assertNotNull(transaction);
        assertEquals(10, transaction.getAmount());
        assertEquals(Transaction.Type.CREDIT, transaction.getType());
        assertEquals("Initial Credit", transaction.getDescription());
    }

    @Test
    public void transfer() {
        Account sender = getRandomAccount(1l, 15);
        Account receiver = getRandomAccount(2l, 0);
        Mockito.when(transactionRepository.save(any(Transaction.class))).then(AdditionalAnswers.returnsFirstArg());
        Mockito.when(accountRepository.save(any(Account.class))).then(AdditionalAnswers.returnsFirstArg());

        target.transfer(getRandomTransferRequest(), sender, receiver);
        assertEquals(5, sender.getBalance());
        assertEquals(1, sender.getTransactions().size());
        assertEquals(Transaction.Type.DEBIT, sender.getTransactions().get(0).getType());

        assertEquals(10, receiver.getBalance());
        assertEquals(1, receiver.getTransactions().size());
        assertEquals(Transaction.Type.CREDIT, receiver.getTransactions().get(0).getType());
    }

    @Test(expected = InvalidAmountException.class)
    public void transferWithInsufficientBalance() {
        target.transfer(getRandomTransferRequest(), getRandomAccount(), getRandomAccount());
    }

    private TransferRequest getRandomTransferRequest() {
        TransferRequest transferRequest = new TransferRequest();
        transferRequest.setAmount(10);
        return transferRequest;
    }

    private Account getRandomAccount() {
        return getRandomAccount(1l, 5);
    }

    private Account getRandomAccount(long accountId, int balance) {
        Account account = new Account();
        account.setAccountId(accountId);
        account.setBalance(balance);
        return account;
    }


}
