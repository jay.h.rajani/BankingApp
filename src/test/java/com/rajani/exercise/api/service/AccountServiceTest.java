package com.rajani.exercise.api.service;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.dao.model.Customer;
import com.rajani.exercise.api.dao.model.Transaction;
import com.rajani.exercise.api.model.CreateAccountRequest;
import com.rajani.exercise.api.repository.AccountRepository;
import com.rajani.exercise.api.repository.CustomerRepository;
import com.rajani.exercise.api.service.impl.AccountServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;

/**
 * Created by jayrajani
 */
@SpringBootTest
public class AccountServiceTest {

    private AccountService target;

    @Mock
    private TransactionService transactionService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AccountRepository accountRepository;

    @Before
    public void init() {
        accountRepository = Mockito.mock(AccountRepository.class);
        customerRepository = Mockito.mock(CustomerRepository.class);
        transactionService = Mockito.mock(TransactionService.class);
        target = new AccountServiceImpl(transactionService, accountRepository, customerRepository);
    }

    @Test
    public void createAccountTestWithDeposit() {

        Mockito.when(transactionService.initialCredit(any(Account.class), anyInt())).then(new Answer<Transaction>() {
            @Override
            public Transaction answer(InvocationOnMock invocationOnMock) throws Throwable {
                Transaction transaction = new Transaction();
                transaction.setAmount(invocationOnMock.getArgument(1));
                return transaction;
            }
        });
        Mockito.when(accountRepository.save(any(Account.class))).then(AdditionalAnswers.returnsFirstArg());
        Mockito.when(customerRepository.findById(anyLong())).thenReturn(Optional.of(new Customer()));

        final Account account = target.createAccount(getRandomCreateAccountRequest(10));
        assertNotNull(account.getTransactions());
        assertEquals(1, account.getTransactions().size());
        assertEquals(10, account.getBalance());
    }

    @Test
    public void createAccountTestWithoutDeposit() {

        Mockito.when(accountRepository.save(any(Account.class))).then(AdditionalAnswers.returnsFirstArg());
        Mockito.when(customerRepository.findById(anyLong())).thenReturn(Optional.of(new Customer()));

        final Account account = target.createAccount(getRandomCreateAccountRequest(null));
        assertNotNull(account.getTransactions());
        assertEquals(0, account.getTransactions().size());
    }

    private CreateAccountRequest getRandomCreateAccountRequest(Integer balance) {
        CreateAccountRequest request = new CreateAccountRequest();
        request.setInitialBalance(balance);
        request.setCustomerId(1l);
        return request;
    }

}
