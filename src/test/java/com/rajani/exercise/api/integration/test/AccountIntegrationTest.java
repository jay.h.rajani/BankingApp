package com.rajani.exercise.api.integration.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by jayrajani
 */

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/account.feature", plugin = {"pretty", "html:target/cucumber"})
public class AccountIntegrationTest {
}
