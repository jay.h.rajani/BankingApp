package com.rajani.exercise.api.integration.test.steps;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.integration.test.ApiClient;
import com.rajani.exercise.api.model.CreateAccountRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Ignore;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;


/**
 * Created by jayrajani
 */
@Ignore
public class AccountFeatureSteps  extends ApiClient {

    private static int httpStatusCode;
    private static Account account;
    private static long sender;
    private static long receiver;

    @When("^I request the account details with accountId (\\d+)$")
    public void i_request_the_account_details_with_accountId(final long accountId) {
        try {
            account = getAccountById(accountId);
        } catch(HttpClientErrorException e) {
            account = null;
            httpStatusCode = e.getStatusCode().value();
        }
    }

    @Then("^I get the account details$")
    public void i_get_the_account_details() {
        assertNotNull(account);
    }

    @And("^the account should contain (\\d+) as accountId$")
    public void the_account_should_contain_the_requested_accountId(long accountId) {
        assertEquals(account.getAccountId(), accountId);
    }

    @When("^I create the account with customerId (\\d+) and balance (\\d+) euro$")
    public void i_create_the_account_with_customerId_and_balance(long customerId, int balance) throws Exception {
        account = createAccount(new CreateAccountRequest(customerId, balance));
    }

    @Then("^the account should contain (\\d+) as customerId$")
    public void the_account_should_contain_as_customerId(long customerId) throws Exception {
        assertNotNull(account.getAccountId());
        assertEquals(account.getCustomer().getCustomerId(), customerId);
    }

    @Then("^the account should have (\\d+) euro balance$")
    public void the_account_should_have_balance(int balance) throws Exception {
        assertEquals(account.getBalance(), balance);
    }

    @Then("^the number of transactions in the account should be (\\d+)$")
    public void the_number_of_transactions_in_the_account_should_be(int numberOfTransactions) throws Exception {
        int numberOfTransactionsActual = account.getTransactions() != null ? account.getTransactions().size() : 0;
        assertEquals(numberOfTransactionsActual, numberOfTransactions);
    }

    @Then("^I receive account not found error$")
    public void i_receive_account_not_found_error() {
        assertNull(account);
        assertEquals(404, httpStatusCode);
    }

    @And("^I use this account as receiver$")
    public void i_use_this_account_as_receiver() throws Exception {
        receiver = account.getAccountId();
    }

    @And("^I use this account as sender")
    public void i_use_this_account_as_sender() throws Exception {
        sender = account.getAccountId();
    }

    @When("^I transfer (\\d+) euro from sender to receiver$")
    public void i_transfer_euro_from_sender_to_receiver(final int amount) {
        transfer(sender, receiver, amount);
    }

    @Then("^the balance of receiver is (\\d+) euro$")
    public void the_balance_of_receiver_is_euro(final int balance) {
        Account account = getAccountById(receiver);
        assertNotNull(account);
        assertEquals(account.getBalance(), balance);
    }

    @And("^the balance of sender is (\\d+) euro$")
    public void the_balance_of_sender_is_euro(final int balance) {
        Account account = getAccountById(sender);
        assertNotNull(account);
        assertEquals(account.getBalance(), balance);
    }

}
