package com.rajani.exercise.api.integration.test;

import com.rajani.exercise.api.dao.model.Account;

import com.rajani.exercise.api.model.CreateAccountRequest;
import com.rajani.exercise.api.model.TransferRequest;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * Created by jayrajani
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiClient {
    private static final Logger LOG = LoggerFactory.getLogger(ApiClient.class);

    private final String SERVER_URL = "http://localhost";
    private final String CUSTOMER_ENDPOINT = "/customers";
    private final String ACCOUNT_ENDPOINT = "/accounts";
    private final String TRANSACTIONS_ENDPOINT = "/transactions";


    private RestTemplate restTemplate;

    @LocalServerPort
    protected int port;

    public ApiClient() {
        restTemplate = new RestTemplate();
    }

    private String customerEndpoint() {
        return SERVER_URL + ":" + port + CUSTOMER_ENDPOINT;
    }

    private String accountsEndpoint() {
        return SERVER_URL + ":" + port + ACCOUNT_ENDPOINT;
    }

    private String transactionsEndpoint() {
        return SERVER_URL + ":" + port + TRANSACTIONS_ENDPOINT;
    }

    protected List<Account> getCustomerAccountsById(Long customerId) {
        LOG.info("Calling customers api with id [{}]", customerId);
        String url = customerEndpoint() + "/" + customerId + "/accounts";
        Account[] accounts = restTemplate.getForObject(url, Account[].class);
        return Arrays.asList(accounts);
    }

    protected Account getAccountById(Long accountId) {
        LOG.info("Calling accounts api with id [{}]", accountId);
        String url = accountsEndpoint() + "/" + accountId;
        ResponseEntity<Account> responseEntity = restTemplate.getForEntity(url, Account.class);
        return responseEntity.getBody();
    }

    protected Account createAccount(CreateAccountRequest request) {
        LOG.info("Calling createAccount [{}]", request);
        return restTemplate.postForEntity(accountsEndpoint(), request, Account.class).getBody();
    }

    protected void transfer(long sender, long receiver, int amount) {
        LOG.info("Calling transfer sender [{}], receiver [{}], amount [{}]", amount);
        TransferRequest request = new TransferRequest(amount, sender, receiver);
        restTemplate.postForObject(transactionsEndpoint(), request, String.class);
    }

    void clean() {
        restTemplate.delete(accountsEndpoint());
    }
}
