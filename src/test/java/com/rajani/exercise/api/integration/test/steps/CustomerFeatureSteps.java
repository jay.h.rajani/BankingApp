package com.rajani.exercise.api.integration.test.steps;

import com.rajani.exercise.api.dao.model.Account;
import com.rajani.exercise.api.integration.test.ApiClient;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Ignore;

import java.util.List;

import static org.junit.Assert.*;


/**
 * Created by jayrajani
 */
@Ignore
public class CustomerFeatureSteps extends ApiClient {

    private static List<Account> accounts;

    @When("^I request the customer details with customerId (\\d+)$")
    public void i_request_the_customer_details_with_customerId(final long customerId) {
        accounts = getCustomerAccountsById(customerId);

    }

    @Then("^I receive the details of (\\d+) accounts$")
    public void the_number_of_accounts_should_be(int numberOfAccounts) throws Exception {
        int numberOfAccountsActual = accounts != null ? accounts.size() : 0;
        assertEquals(numberOfAccountsActual, numberOfAccounts);
    }
}
