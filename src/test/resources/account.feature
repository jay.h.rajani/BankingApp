Feature: Account APIs

  Scenario: Create Account with initial balance
    When I create the account with customerId 1 and balance 10 euro
    Then the account should contain 1 as customerId
    And the account should have 10 euro balance
    And the number of transactions in the account should be 1

    When I request the account details with accountId 1
    Then I get the account details
    And the account should contain 1 as accountId

  Scenario: Get account details for an invalid id
    When I request the account details with accountId 111
    Then I receive account not found error

  Scenario: Transfer money from one to another
    When I create the account with customerId 1 and balance 10 euro
    Then the account should contain 1 as customerId
    And I use this account as sender

    When I create the account with customerId 2 and balance 0 euro
    Then the account should contain 2 as customerId
    And I use this account as receiver

    When I transfer 5 euro from sender to receiver
    Then the balance of receiver is 5 euro
    And the balance of sender is 5 euro
