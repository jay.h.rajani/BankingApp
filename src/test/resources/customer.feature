Feature: Customer APIs

  Scenario: Customer can create multiple accounts
    When I create the account with customerId 3 and balance 10 euro
    Then the account should contain 3 as customerId

    When I create the account with customerId 3 and balance 10 euro
    Then the account should contain 3 as customerId

    When I request the customer details with customerId 3
    Then I receive the details of 2 accounts

